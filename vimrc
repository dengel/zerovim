execute pathogen#infect()
set background=dark
set history=700                             "Sets how many lines of history VIM has to remember
set ttimeoutlen=50                          "Speed up O etc in the Terminal
set autoread                                "Set to auto read when a file is changed from the outside
set bs=2                                    "allow backspace
set scrolloff=6                             "start scrolling 5 lines before edge of viewport
set pastetoggle=<F10>                       "Better paste behavior

map <F5> :setlocal spell! spelllang=en_us<CR>

map <F6> :set list!<CR>
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
highlight SpecialKey term=standout ctermbg=yellow guibg=yellow
highlight RedundantSpaces term=standout ctermbg=Grey guibg=#ffddcc

set showmatch " show matching brackets (),{},[]
set mat=5 " show matching brackets for 0.5 seconds

"Search Options
set ignorecase "Ignore case when searching
set incsearch  "Make search act like search in modern browsers
set magic      "Set magic on, for regular expressions

"Enable filetype plugin
filetype plugin on
filetype indent on
filetype on
syntax enable

"Turn backup off
set nobackup
set nowb
set noswapfile

"Tabs
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab

"Line Wrapping
set wrap   "Wrap lines

" Make this thing work!
let g:Powerline_symbols = 'unicode'
let &t_Co=256
set encoding=utf-8
set fillchars+=stl:\ ,stlnc:\
set laststatus=2

let g:airline#extensions#tabline#enabled = 1

nnoremap <Leader>l :ls<CR>
nnoremap <Leader>b :bp<CR>
nnoremap <Leader>f :bn<CR>
nnoremap <Leader>g :e#<CR>
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>

nnoremap <PageUp>   :bprevious<CR>
nnoremap <PageDown> :bnext<CR>
