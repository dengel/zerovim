#!/usr/bin/env bash

echo "*** Installing vimrc..."
mv ~/.vimrc ~/.vimrc-$$
ln -s ~/.vim/vimrc ~/.vimrc

echo "*** Installing modules..."
mkdir bundle
while read LINE; do
  echo "    Cloning: $LINE"
  (cd bundle && git clone $LINE)
done < bundles.txt
