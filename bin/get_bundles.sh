#!/usr/bin/env bash
for dir in ~/.vim/bundle/*; do (cd $dir && git remote -v); done | awk '{print $2}' | sort -u
